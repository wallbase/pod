#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

kubednsautoscaler_version=1.1.1
dns_version=1.14.2
kube_pause_version=3.0
dashboard_version=v1.6.3
fluentd_es_version=1.22
kibana_version=5.6.4
elasticsearch_version=v5.6.4
heapster_version=v1.4.0
heapster_grafana_version=v4.4.1
heapster_influxdb_version=v1.1.1
nginx_ingress_version=0.9.0-beta.11
defaultbackend_version=1.3

helm_version=v2.9.1

GCR_URL=gcr.io/google-containers

CONTAINER_IMAGE=wallbase/pod

images=(
    cluster-proportional-autoscaler-amd64:${kubednsautoscaler_version}
    k8s-dns-sidecar-amd64:${dns_version}
    k8s-dns-kube-dns-amd64:${dns_version}
    k8s-dns-dnsmasq-nanny-amd64:${dns_version}
    pause-amd64:${kube_pause_version}
    kubernetes-dashboard-amd64:${dashboard_version}
    fluentd-elasticsearch:${fluentd_es_version}
    kibana:${kibana_version}
    elasticsearch:${elasticsearch_version}
    fluentd-elasticsearch:${fluentd_es_version}
    kibana:${kibana_version}
    heapster-amd64:${heapster_version}
    heapster-grafana-amd64:${heapster_grafana_version}
    heapster-influxdb-amd64:${heapster_influxdb_version}
    nginx-ingress-controller:${nginx_ingress_version}
    defaultbackend:${defaultbackend_version}
)

for imageName in ${images[@]} ; do
  docker pull $GCR_URL/$imageName
  docker tag $GCR_URL/$imageName $CONTAINER_IMAGE/$imageName
  docker push $CONTAINER_IMAGE/$imageName
done
