#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

QUAY_URL=quay.io
CONTAINER_IMAGE=wallbase/pod

images=(
    coreos/etcd:v3.2.18
    coreos/flannel:v0.10.0
    coreos/flannel-cni:v0.3.0
    calico/ctl:v1.6.3
    calico/node:v2.6.8
    calico/cni:v1.11.4
    calico/kube-controllers:v1.0.3
    calico/routereflector:v0.4.2
)

for imageName in ${images[@]} ; do
  docker pull $QUAY_URL/$imageName
  docker tag $QUAY_URL/$imageName $CONTAINER_IMAGE/${imageName}
  docker push $CONTAINER_IMAGE/${imageName}
done
